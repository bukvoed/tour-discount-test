<p align="center">
    <h1 align="center">Test task: Calculate discounted tour price</h1>
    <br>
</p>

INSTALLATION
------------

### Install from Gitlab repository

Download repo

~~~
git clone https://gitlab.com/bukvoed/tour-discount-test.git
~~~

Install packages

~~~
composer install
composer require symfony/serializer-pack
~~~

USAGE
-------------

### cURL

```bash
curl --location 'localhost:8000/tour/calculate' \
--header 'Content-Type: application/json' \
--data '{
    "price": 10000,
    "birthDate": "25.01.2000",
    "startDate": "15.01.2024",
    "paidDate": "30.08.2023"
}'
```
