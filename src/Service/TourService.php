<?php

namespace App\Service;

use App\Model\DiscountRule;
use App\Model\Tour;

class TourService
{
    public function __construct(private Tour $tour, private array $discountRules)
    {
    }

    public function calculateDiscountedPrice(): float
    {
        foreach ($this->discountRules as $discountRule) {
            $this->applyDiscountRule($discountRule);
        }
        return $this->tour->getPrice();
    }

    public function applyDiscountRule(DiscountRule $discountRule): void
    {
        $this->tour->setPrice($discountRule->execute($this->tour));
    }
}
