<?php
namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class TourDTO
{
    public function __construct(
        #[Assert\NotBlank]
        public float $price,
        #[Assert\NotBlank]
        public string $birthDate,
        #[Assert\NotBlank]
        public string $startDate,
        #[Assert\NotBlank]
        public string $paidDate,
    ) {
    }
}
