<?php

namespace App\Model;

use App\Model\DiscountRule;
use App\Helper\DateHelper;

class EarlyBookingDiscountRule implements DiscountRule
{
    public function execute(Tour $tour): float
    {
        $discountPercent = 0;
        $yearsDiff = DateHelper::getYearsDiff($tour->getPaidDate(), $tour->getStartDate());
        if (DateHelper::isBetween($tour->getStartDate(), '0401', '0930')) {
            if ($yearsDiff == 1 && DateHelper::isBetween($tour->getPaidDate(), '0101', '1130')) {
                $discountPercent = 7;
            } elseif ($yearsDiff == 1 && DateHelper::isBetween($tour->getPaidDate(), '1201', '1231')) {
                $discountPercent = 5;
            } elseif ($yearsDiff == 0 && DateHelper::isBetween($tour->getPaidDate(), '0101', '0131')) {
                $discountPercent = 3;
            }
        }
        if (DateHelper::isBetween($tour->getStartDate(), '1001', '1231')) {
            if ($yearsDiff == 0 && DateHelper::isBetween($tour->getPaidDate(), '0101', '0331')) {
                $discountPercent = 7;
            } elseif ($yearsDiff == 0 && DateHelper::isBetween($tour->getPaidDate(), '0401', '0430')) {
                $discountPercent = 5;
            } elseif ($yearsDiff == 0 && DateHelper::isBetween($tour->getPaidDate(), '0501', '0531')) {
                $discountPercent = 3;
            }
        }
        if (DateHelper::isBetween($tour->getStartDate(), '0101', '0114')) {
            if ($yearsDiff == 1 && DateHelper::isBetween($tour->getPaidDate(), '0101', '0331')) {
                $discountPercent = 7;
            } elseif ($yearsDiff == 1 && DateHelper::isBetween($tour->getPaidDate(), '0401', '0430')) {
                $discountPercent = 5;
            } elseif ($yearsDiff == 1 && DateHelper::isBetween($tour->getPaidDate(), '0501', '0531')) {
                $discountPercent = 3;
            }
        }
        if (DateHelper::isBetween($tour->getStartDate(), '0115', '1231')) {
            if ($yearsDiff == 1 && DateHelper::isBetween($tour->getPaidDate(), '0101', '0831')) {
                $discountPercent = 7;
            } elseif ($yearsDiff == 1 && DateHelper::isBetween($tour->getPaidDate(), '0901', '0930')) {
                $discountPercent = 5;
            } elseif ($yearsDiff == 1 && DateHelper::isBetween($tour->getPaidDate(), '1001', '1031')) {
                $discountPercent = 3;
            }
        }
        $discount = min(1500, $tour->getPrice() * $discountPercent / 100);
        return $tour->getPrice() - $discount;
    }
}
