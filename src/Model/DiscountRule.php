<?php

namespace App\Model;

interface DiscountRule
{
    public function execute(Tour $tour): float;
}
