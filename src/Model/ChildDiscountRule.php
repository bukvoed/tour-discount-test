<?php

namespace App\Model;

use App\Model\DiscountRule;
use App\Helper\DateHelper;

class ChildDiscountRule implements DiscountRule
{
    public const ADULT_MIN_YEARS = 18;

    public function execute(Tour $tour): float
    {
        if (!$this->isUserChild($tour)) {
            return $tour->getPrice();
        }
        $age = DateHelper::getAge($tour->getBirthDate(), $tour->getStartDate());
        $discount = match ($age) {
            3, 4, 5 => $tour->getPrice() * 80 / 100,
            6, 7, 8, 9, 10, 11 => min(4500, $tour->getPrice() * 30 / 100),
            12, 13, 14, 15, 16, 17, 18 => $tour->getPrice() * 10 / 100,
            default => 0
        };
        return $tour->getPrice() - $discount;
    }

    public function isUserChild(Tour $tour): bool
    {
        $age = DateHelper::getAge($tour->getBirthDate(), $tour->getStartDate());
        return $age < self::ADULT_MIN_YEARS;
    }
}
