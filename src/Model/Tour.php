<?php

namespace App\Model;

class Tour
{
    public function __construct(
        private float  $price,
        private string $birthDate,
        private string $startDate,
        private string $paidDate,
    ) {
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getBirthDate(): string
    {
        return $this->birthDate;
    }

    /**
     * @param string $birthDate
     */
    public function setBirthDate(string $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate(string $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return string
     */
    public function getPaidDate(): string
    {
        return $this->paidDate;
    }

    /**
     * @param string $paidDate
     */
    public function setPaidDate(string $paidDate): void
    {
        $this->paidDate = $paidDate;
    }
}
