<?php

namespace App\Controller;

use App\Model\ChildDiscountRule;
use App\Model\EarlyBookingDiscountRule;
use App\Model\Tour;
use App\Model\TourDTO;
use App\Service\TourService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class TourController extends AbstractController
{
    /** @noinspection PhpUnused */
    #[Route('tour/calculate', format: 'json')]
    public function calculate(
        #[MapRequestPayload(acceptFormat: 'json')] TourDTO $tourDto
    ): JsonResponse {
        $tour = new Tour($tourDto->price, $tourDto->birthDate, $tourDto->startDate, $tourDto->paidDate);
        $tourService = new TourService($tour, [new ChildDiscountRule(), new EarlyBookingDiscountRule()]);
        $data = ['discountedPrice' => $tourService->calculateDiscountedPrice()];
        return $this->json($data);
    }
}
