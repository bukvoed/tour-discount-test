<?php

namespace App\Helper;

use DateTimeImmutable;

class DateHelper
{
    /**
     * Возвращает возраст в годах на дату $byDate
     *
     * @param string $birthday
     * @param string|null $byDate
     * @return int
     */
    public static function getAge(string $birthday, string $byDate = null): int
    {
        $birthday = new DateTimeImmutable($birthday);
        $byDate = new DateTimeImmutable($byDate ?? 'now');
        return $byDate->diff($birthday)->y;
    }

    /**
     * Возвращает разность годов у двух дат
     *
     * @param string $dateFrom
     * @param string $dateTill
     * @return int
     */
    public static function getYearsDiff(string $dateFrom, string $dateTill): int
    {
        $dateFrom = new DateTimeImmutable($dateFrom);
        $dateTill = new DateTimeImmutable($dateTill);
        return $dateTill->format('Y') - $dateFrom->format('Y');
    }

    /**
     *
     * Проверяет, попадает ли дата в диапазон дат
     *
     * @param string $checkDate
     * @param string $dateFrom format "md"
     * @param string $dateTill format "md"
     * @return bool
     */
    public static function isBetween(string $checkDate, string $dateFrom, string $dateTill): bool
    {
        $checkDateWithoutYear = (new DateTimeImmutable($checkDate))->format('md');
        return $checkDateWithoutYear >= $dateFrom && $checkDateWithoutYear <= $dateTill;
    }
}
